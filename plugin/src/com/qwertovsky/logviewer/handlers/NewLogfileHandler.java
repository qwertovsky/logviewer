package com.qwertovsky.logviewer.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

import com.qwertovsky.logviewer.model.ILogViewerService;
import com.qwertovsky.logviewer.model.LogFile;
import com.qwertovsky.logviewer.parts.LogsExplorerPart;
import com.qwertovsky.logviewer.wizards.LogfileWizard;

public class NewLogfileHandler {

	@Execute
	public void createNewLogfile(Shell shell, EPartService partService,
			ILogViewerService logViewerService) {
		LogsExplorerPart part = (LogsExplorerPart) partService.findPart(LogsExplorerPart.ID).getObject();
		LogFile logFile = new LogFile();
		WizardDialog dialog = new WizardDialog(shell, new LogfileWizard(logFile, "New file"));
		if (dialog.open() == WizardDialog.OK) {
			logViewerService.saveLogFile(logFile);
			part.getTableViewer().setInput(logViewerService.getLogFiles());
			part.getTableViewer().refresh();
		}
	}
}
