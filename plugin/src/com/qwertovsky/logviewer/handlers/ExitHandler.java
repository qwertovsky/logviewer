package com.qwertovsky.logviewer.handlers;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.IWorkbench;

public class ExitHandler {

	@Execute
	public void exit(IWorkbench workbench) {
		System.out.println("exit");
		workbench.close();
	}
}
