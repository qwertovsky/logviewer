package com.qwertovsky.logviewer.handlers;

import org.eclipse.e4.core.di.annotations.Execute;

import com.qwertovsky.logviewer.model.ILogViewerService;

public class SaveLogfileHandler {

	@Execute
	public void saveLogfile(ILogViewerService logViewerService) {
		logViewerService.saveFileList();
	}
}
