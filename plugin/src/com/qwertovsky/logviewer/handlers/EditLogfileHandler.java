package com.qwertovsky.logviewer.handlers;


import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.jface.wizard.WizardDialog;
import org.eclipse.swt.widgets.Shell;

import com.qwertovsky.logviewer.model.ILogViewerService;
import com.qwertovsky.logviewer.model.LogFile;
import com.qwertovsky.logviewer.parts.LogsExplorerPart;
import com.qwertovsky.logviewer.wizards.LogfileWizard;

public class EditLogfileHandler {

	@Execute
	public void editLogfile(ILogViewerService logViewerService
			, EPartService partService, Shell shell) {
		LogsExplorerPart part = (LogsExplorerPart) partService.findPart(LogsExplorerPart.ID).getObject();
		LogFile logFile = (LogFile) part.getTableViewer().getStructuredSelection().getFirstElement();
		WizardDialog dialog = new WizardDialog(shell, new LogfileWizard(logFile, "Edit file"));
		if (dialog.open() == WizardDialog.OK) {
			logViewerService.saveLogFile(logFile);
			part.getTableViewer().setInput(logViewerService.getLogFiles());
			part.getTableViewer().refresh();
		}
	}
}
