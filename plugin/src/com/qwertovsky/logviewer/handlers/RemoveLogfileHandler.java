package com.qwertovsky.logviewer.handlers;

import java.util.Iterator;

import org.eclipse.e4.core.di.annotations.Execute;
import org.eclipse.e4.ui.workbench.modeling.EPartService;

import com.qwertovsky.logviewer.model.ILogViewerService;
import com.qwertovsky.logviewer.model.LogFile;
import com.qwertovsky.logviewer.parts.LogsExplorerPart;

public class RemoveLogfileHandler {

	@Execute
	public void removeLogfile(ILogViewerService logViewerService
			, EPartService partService) {
		LogsExplorerPart part = (LogsExplorerPart) partService.findPart(LogsExplorerPart.ID).getObject();
		@SuppressWarnings("unchecked")
		Iterator<LogFile> iterator = part.getTableViewer().getStructuredSelection().iterator();
		while (iterator.hasNext()) {
			LogFile file = iterator.next();
			logViewerService.deleteLogFile(file.getId());
			part.getTableViewer().setInput(logViewerService.getLogFiles());
			part.getTableViewer().refresh();
		}
		
	}
}
