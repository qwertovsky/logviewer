package com.qwertovsky.logviewer;

import org.eclipse.jface.text.ITextStore;

public class TailedTextStore implements ITextStore {
	
	StringBuffer content = new StringBuffer("");

	@Override
	public char get(int offset) {
		return content.charAt(offset);
	}

	@Override
	public String get(int offset, int length) {
		return content.substring(offset, offset + length);
	}

	@Override
	public int getLength() {
		return content.length();
	}

	@Override
	public void replace(int offset, int length, String text) {
		content.replace(offset, offset + length, text);
	}

	@Override
	public void set(String text) {
		content = new StringBuffer(text);
	}

	/**
	 * Append text to the end
	 * @param text
	 */
	public void append(String text) {
		content.append(text);
	}
}
