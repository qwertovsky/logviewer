package com.qwertovsky.logviewer.wizards;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.conversion.Converter;
import org.eclipse.core.databinding.conversion.IConverter;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.qwertovsky.logviewer.model.LogFile;

public class LogfileWizardSFTPPage extends WizardPage {

	static final String PAGENAME = "path page";
	private LogFile file;
	private DataBindingContext bindCtx = new DataBindingContext();
	private Binding hostBinding;
	private Binding portBinding;
	private Binding pathBinding;
	private Binding userBinding;
	private Binding passBinding;

	protected LogfileWizardSFTPPage(LogFile file) {
		super(PAGENAME);
		setTitle("File path information");
		setDescription("Specify file location");
		setPageComplete(false);

		this.file = file;
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout(1, true);
		layout.marginLeft = 10;
		container.setLayout(layout);
		setControl(container);
		
		drawWidgets();
	}

	private void drawWidgets() {
		Composite container = (Composite) getControl();
		Label hostLabel = new Label(container, SWT.NULL);
		hostLabel.setText("Host:");
		Text hostText = new Text(container, SWT.BORDER);
		GridData hostLD = new GridData();
		hostLD.grabExcessHorizontalSpace = true;
		hostLD.horizontalAlignment = SWT.FILL;
		hostText.setLayoutData(hostLD);
		hostBinding = createBinding(hostText, "host", null, new EmptyValueValidator("Host can not be empty"));
		
		Label portLabel = new Label(container, SWT.NULL);
		portLabel.setText("Port:");
		Text portText = new Text(container, SWT.BORDER);
		GridData portLD = new GridData();
		portLD.grabExcessHorizontalSpace = true;
		portLD.horizontalAlignment = SWT.FILL;
		portText.setLayoutData(portLD);
		IConverter portConverter = new Converter(String.class, Integer.class) {
			@Override
			public Object convert(Object fromObject) {
				String value = (String) fromObject;
				if (value != null && !value.trim().isEmpty()) {
					try {
						return Integer.parseInt(value);
					} catch (NumberFormatException ex) {
						return fromObject;
					}
				}
				return null;
			}
		};
		IValidator portValidator = new IValidator() {
			@Override
			public IStatus validate(Object value) {
				if (!(value instanceof Integer)) {
					return ValidationStatus.error("Port should be a number");
				}
				Integer valueI = (Integer) value;
				if (valueI != null && valueI > 0) {
					return ValidationStatus.ok();
				}
				return ValidationStatus.error("Port must be more then 0");
			}
		};
		portBinding = createBinding(portText, "port", portConverter, portValidator);
		
		Label pathLabel = new Label(container, SWT.NULL);
		pathLabel.setText("Path:");
		Text pathText = new Text(container, SWT.BORDER);
		GridData pathLD = new GridData();
		pathLD.grabExcessHorizontalSpace = true;
		pathLD.horizontalAlignment = SWT.FILL;
		pathText.setLayoutData(pathLD);
		pathBinding = createBinding(pathText, "path", null, new EmptyValueValidator("Path can not be empty"));
		
		Label userLabel = new Label(container, SWT.NULL);
		userLabel.setText("Username:");
		Text userText = new Text(container, SWT.BORDER);
		GridData userLD = new GridData();
		userLD.grabExcessHorizontalSpace = true;
		userLD.horizontalAlignment = SWT.FILL;
		userText.setLayoutData(userLD);
		userBinding = createBinding(userText, "username", null, new EmptyValueValidator("Username can not be empty"));
		
		Label passLabel = new Label(container, SWT.NULL);
		passLabel.setText("Password:");
		Text passText = new Text(container, SWT.BORDER | SWT.PASSWORD);
		GridData passLD = new GridData();
		passLD.grabExcessHorizontalSpace = true;
		passLD.horizontalAlignment = SWT.FILL;
		passText.setLayoutData(passLD);
		passBinding = createBinding(passText, "password", null, null);
	}

	@SuppressWarnings("unchecked")
	private Binding createBinding(Text text, String propertyName
			, IConverter converter, IValidator validator) {
		IObservableValue<String> textO = WidgetProperties.text(SWT.Modify).observe(text);
		IObservableValue<String> modelO = BeanProperties
			.value(LogFile.class, propertyName).observe(this.file);
		UpdateValueStrategy strategy = new UpdateValueStrategy();
		strategy.setConverter(converter);
		strategy.setBeforeSetValidator(validator);
		Binding binding = bindCtx.bindValue(textO, modelO, strategy, null);
		binding.getValidationStatus().addValueChangeListener(event -> getWizard().getContainer().updateButtons());
		ControlDecorationSupport.create(binding, SWT.TOP | SWT.LEFT);
		return binding;
	}

	@Override
	public boolean isPageComplete() {
		boolean complete = ((IStatus)hostBinding.getValidationStatus().getValue()).isOK()
				&& ((IStatus)portBinding.getValidationStatus().getValue()).isOK()
				&& ((IStatus)pathBinding.getValidationStatus().getValue()).isOK()
				&& ((IStatus)userBinding.getValidationStatus().getValue()).isOK()
				&& ((IStatus)passBinding.getValidationStatus().getValue()).isOK()
				;
		return complete;
	}
	
	private class EmptyValueValidator implements IValidator {
		private String message;

		EmptyValueValidator(String message) {
			this.message = message;
		}

		@Override
		public IStatus validate(Object value) {
			String valueS = (String) value;
			if (valueS != null && !valueS.trim().isEmpty()) {
				return ValidationStatus.ok();
			}
			return ValidationStatus.error(message);
		}
	};

}
