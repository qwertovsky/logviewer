package com.qwertovsky.logviewer.wizards;

import org.eclipse.core.databinding.Binding;
import org.eclipse.core.databinding.DataBindingContext;
import org.eclipse.core.databinding.UpdateValueStrategy;
import org.eclipse.core.databinding.beans.BeanProperties;
import org.eclipse.core.databinding.observable.value.IObservableValue;
import org.eclipse.core.databinding.validation.IValidator;
import org.eclipse.core.databinding.validation.ValidationStatus;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.jface.databinding.fieldassist.ControlDecorationSupport;
import org.eclipse.jface.databinding.swt.WidgetProperties;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.qwertovsky.logviewer.model.LogFile;

public class LogfileWizardLocalPage extends WizardPage {

	static final String PAGENAME = "localTypePage";
	private LogFile file;
	private DataBindingContext bindCtx = new DataBindingContext();
	private Binding pathBinding;

	protected LogfileWizardLocalPage(LogFile file) {
		super(PAGENAME);
		setTitle("File path information");
		setDescription("Specify file location");
		setPageComplete(false);

		this.file = file;
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout(2, false);
		layout.marginLeft = 10;
		container.setLayout(layout);
		setControl(container);
		
		drawWidgets();
	}

	@SuppressWarnings("unchecked")
	private void drawWidgets() {
		Composite container = (Composite) getControl();
		Label pathLabel = new Label(container, SWT.NULL);
		GridData labelLD = new GridData();
		labelLD.horizontalSpan = 2;
		pathLabel.setLayoutData(labelLD);
		pathLabel.setText("Path:");

		Text pathText = new Text(container, SWT.BORDER);
		GridData pathLD = new GridData();
		pathLD.grabExcessHorizontalSpace = true;
		pathLD.horizontalAlignment = SWT.FILL;
		pathText.setLayoutData(pathLD);
		
		Button fileDialogBtn = new Button(container, SWT.BORDER);
		fileDialogBtn.setText("...");
		fileDialogBtn.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fileDialog = new FileDialog(container.getShell());
				fileDialog.setText("Select logfile");
				String selected = fileDialog.open();
				file.setPath(selected);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
			}
		});
		
		IObservableValue<String> pathTextO = WidgetProperties.text(SWT.Modify).observe(pathText);
		IObservableValue<String> pathModelO = BeanProperties
			.value(LogFile.class, "path").observe(this.file);
		UpdateValueStrategy pathStrategy = new UpdateValueStrategy();
		pathStrategy.setBeforeSetValidator(new IValidator() {
			@Override
			public IStatus validate(Object value) {
				String valueS = (String) value;
				if (valueS != null && !valueS.trim().isEmpty()) {
					return ValidationStatus.ok();
				}
				return ValidationStatus.error("Path can not be empty");
			}
		});
		pathBinding = bindCtx.bindValue(pathTextO, pathModelO, pathStrategy, null);
		pathBinding.getValidationStatus().addValueChangeListener( event -> getWizard().getContainer().updateButtons());
		ControlDecorationSupport.create(pathBinding, SWT.TOP | SWT.LEFT);
	}

	@Override
	public boolean isPageComplete() {
		boolean complete = ((IStatus)pathBinding.getValidationStatus().getValue()).isOK();
		return complete;
	}

}
