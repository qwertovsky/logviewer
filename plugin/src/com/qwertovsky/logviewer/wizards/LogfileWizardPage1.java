package com.qwertovsky.logviewer.wizards;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import com.qwertovsky.logviewer.model.FileSystemType;
import com.qwertovsky.logviewer.model.LogFile;

public class LogfileWizardPage1 extends WizardPage {

	static final String PAGENAME = "base page";
	private LogFile file;

	protected LogfileWizardPage1(LogFile file) {
		super(PAGENAME);
		setTitle("Base file information");
		setDescription("Select file name and type");
		setPageComplete(false);

		this.file = file;
	}

	@Override
	public void createControl(Composite parent) {
		Composite container = new Composite(parent, SWT.NULL);
		GridLayout layout = new GridLayout(1, true);
		container.setLayout(layout);
		setControl(container);
		
		Label nameLabel = new Label(container, SWT.NULL);
		nameLabel.setText("Name:");
		Text nameText = new Text(container, SWT.BORDER);
		nameText.setMessage("How file will be displayed in list");
		if (file.getName() != null) {
			nameText.setText(file.getName());
		}
		GridData nameLD = new GridData();
		nameLD.grabExcessHorizontalSpace = true;
		nameLD.horizontalAlignment = SWT.FILL;
		nameText.setLayoutData(nameLD);
		nameText.addModifyListener(new ModifyListener() {
			@Override
			public void modifyText(ModifyEvent e) {
				file.setName(nameText.getText());
				getWizard().getContainer().updateButtons();
			}
		});
		Label typeLabel = new Label(container, SWT.NULL);
		typeLabel.setText("Type:");
		ComboViewer typeCombo = new ComboViewer(container);
		typeCombo.setContentProvider(ArrayContentProvider.getInstance());
		typeCombo.setInput(FileSystemType.values());
		if (file.getFileSystem() != null) {
			typeCombo.setSelection(new StructuredSelection(file.getFileSystem()));
		}
		typeCombo.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				FileSystemType type = (FileSystemType) event.getStructuredSelection().getFirstElement();
				file.setFileSystem(type);
				file.clearPathInfo();
				getWizard().getContainer().updateButtons();
			}
		});
		
	}

	@Override
	public boolean isPageComplete() {
		return file.getFileSystem() != null
				&& file.getName() != null && !file.getName().trim().isEmpty();
	}
	
}
