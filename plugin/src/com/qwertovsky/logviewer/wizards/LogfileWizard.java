package com.qwertovsky.logviewer.wizards;

import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import com.qwertovsky.logviewer.model.LogFile;

public class LogfileWizard extends Wizard {

	private LogFile file;
	private LogfileWizardPage1 page1;
	private LogfileWizardLocalPage localPage;
	private LogfileWizardSFTPPage sftpPage;

	public LogfileWizard(LogFile file, String title) {
		setWindowTitle(title);
		this.file = file;
		page1 = new LogfileWizardPage1(file);
		localPage = new LogfileWizardLocalPage(file);
		sftpPage = new LogfileWizardSFTPPage(file);
	}

	@Override
	public void addPages() {
		addPage(page1);
		addPage(localPage);
		addPage(sftpPage);
    }

	@Override
	public boolean performFinish() {
		return true;
	}
	
	@Override
	public IWizardPage getNextPage(IWizardPage page) {
		if (page.equals(page1)) {
			switch (file.getFileSystem()) {
			case LOCAL:
				return localPage;
			case SFTP:
				return sftpPage;
			}
		}
		return null;
	}

	@Override
	public boolean canFinish() {
		if (file.getFileSystem() == null) {
			return false;
		}
		switch (file.getFileSystem()) {
		case LOCAL:
			return localPage.isPageComplete();
		case SFTP:
			return sftpPage.isPageComplete();
		}
        return false;
    }

}
