package com.qwertovsky.logviewer.parts;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.inject.Inject;

import org.eclipse.e4.ui.model.application.ui.MElementContainer;
import org.eclipse.e4.ui.model.application.ui.MUIElement;
import org.eclipse.e4.ui.model.application.ui.basic.MBasicFactory;
import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.model.application.ui.basic.MPartStack;
import org.eclipse.e4.ui.services.EMenuService;
import org.eclipse.e4.ui.workbench.modeling.EModelService;
import org.eclipse.e4.ui.workbench.modeling.EPartService;
import org.eclipse.e4.ui.workbench.modeling.EPartService.PartState;
import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;

import com.qwertovsky.logviewer.model.ILogViewerService;
import com.qwertovsky.logviewer.model.LogFile;

public class LogsExplorerPart {
	public static final String ID = "com.qwertovsky.logviewer.part.logsexplorer";
	
	@Inject
	private MPart logsExplorer;
	@Inject
	private EModelService modelService;
	@Inject
	private EPartService partService;
	 
	private TableViewer tableViewer;

	public LogsExplorerPart() {
		System.out.println("logs explorer constructor");
	}
	
	@PostConstruct
	public void init(Composite parent, ILogViewerService logViewerService, EMenuService menuService) {
		MElementContainer<MUIElement> mainContainer = logsExplorer.getParent().getParent();
		MPartStack contentViewerStack = (MPartStack) modelService.find(
				"com.qwertovsky.logviewer.partstack.viewer", mainContainer);
		
		List<LogFile> files = logViewerService.getLogFiles();
		
//		logsTree = new TreeViewer(parent);
//		logsTree.setContentProvider(provider);
//		logsTree.getTree().setLayoutData(layoutData);
		tableViewer = new TableViewer(parent);

		tableViewer.setContentProvider(ArrayContentProvider.getInstance());
		tableViewer.setLabelProvider(new TableLabelProvader());
		tableViewer.setInput(files);
		tableViewer.getTable().setLayoutData(new GridData(GridData.FILL_BOTH));
		tableViewer.addDoubleClickListener(new IDoubleClickListener() {
			
			@Override
			public void doubleClick(DoubleClickEvent event) {
				MPart fileViewPart = MBasicFactory.INSTANCE.createPart();
				fileViewPart.setContributionURI("bundleclass://com.qwertovsky.logviewer/com.qwertovsky.logviewer.parts.LogContentPart");
				fileViewPart.setCloseable(true);
				fileViewPart.getTags().add(EPartService.REMOVE_ON_HIDE_TAG);

				LogFile file = (LogFile) ((StructuredSelection)event.getSelection()).getFirstElement();
				fileViewPart.getTransientData().put("logFile", file);

				contentViewerStack.getChildren().add(fileViewPart);
				
				partService.showPart(fileViewPart, PartState.ACTIVATE);
			}
		});
		
		menuService.registerContextMenu(tableViewer.getControl(), "com.qwertovsky.logviewer.popupmenu.table");
	}

	public TableViewer getTableViewer() {
		return tableViewer;
	}
	
	private class TableLabelProvader implements ITableLabelProvider {

		@Override
		public void addListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void dispose() {
			// TODO Auto-generated method stub
			
		}

		@Override
		public boolean isLabelProperty(Object element, String property) {
			// TODO Auto-generated method stub
			return false;
		}

		@Override
		public void removeListener(ILabelProviderListener listener) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public Image getColumnImage(Object element, int columnIndex) {
			// TODO Auto-generated method stub
			return null;
		}

		@Override
		public String getColumnText(Object element, int columnIndex) {
			LogFile logfile = (LogFile) element;
			return logfile.getName();
		}
		
	}

}
