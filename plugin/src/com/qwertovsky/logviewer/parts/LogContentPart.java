package com.qwertovsky.logviewer.parts;


import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.inject.Inject;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.eclipse.e4.ui.model.application.ui.basic.MPart;
import org.eclipse.e4.ui.services.IStylingEngine;
import org.eclipse.jface.resource.JFaceResources;
import org.eclipse.jface.resource.LocalResourceManager;
import org.eclipse.jface.text.source.CompositeRuler;
import org.eclipse.jface.text.source.LineNumberRulerColumn;
import org.eclipse.jface.text.source.OverviewRuler;
import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.graphics.RGB;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Text;

import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.qwertovsky.logviewer.LogDocument;
import com.qwertovsky.logviewer.TailedTextStore;
import com.qwertovsky.logviewer.listeners.LogTailerListener;
import com.qwertovsky.logviewer.model.LogFile;
import com.qwertovsky.logviewer.tailer.LocalTailer;
import com.qwertovsky.logviewer.tailer.SftpTailer;

public class LogContentPart {
	private static Logger LOG = LogManager.getLogger(LogContentPart.class);

	private LogFile logFile;
	private Thread tailerTread;
	
	@Inject
	private MPart logContentPart;
	
	@Inject
	private IStylingEngine stylingEngine;
	
	@PostConstruct
	public void init(Composite parent) {
		
		LocalResourceManager resManager =
				new LocalResourceManager(JFaceResources.getResources(), parent);
		
		GridLayout layout = new GridLayout(1, true);
		parent.setLayout(layout);

		CompositeRuler vruler = new CompositeRuler(10);
		LineNumberRulerColumn lineNumberRulerColumn = new LineNumberRulerColumn();
		vruler.addDecorator(0, lineNumberRulerColumn);
		OverviewRuler oruler = new OverviewRuler(null, 20, null);
		SourceViewer source = new SourceViewer(parent, vruler, oruler, true, SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		StyledText sourceText = source.getTextWidget();
		LogDocument document = new LogDocument(new TailedTextStore());
		source.setDocument(document);
		source.setEditable(true);
		GridData sourceLD = new GridData();
		sourceLD.horizontalAlignment = SWT.FILL;
		sourceLD.grabExcessHorizontalSpace = true;
		sourceLD.grabExcessVerticalSpace = true;
		sourceLD.verticalAlignment = SWT.FILL;
		source.getControl().setLayoutData(sourceLD);
		stylingEngine.setClassname(sourceText, "log_source_viewer");

		stylingEngine.setClassname(lineNumberRulerColumn.getControl(), "line_number_column");
		Color lineNumberColor = resManager.createColor(new RGB(180, 180, 180));
		Color lineNumberBackgroundColor = resManager.createColor(new RGB(230, 230, 230));
		lineNumberRulerColumn.setForeground(lineNumberColor);
		lineNumberRulerColumn.setBackground(lineNumberBackgroundColor);

		this.logFile = (LogFile) logContentPart.getTransientData().get("logFile");
		logContentPart.setLabel(logFile.getName());

		loadFile(logFile, source);
		
		
		Composite searchRow = new Composite(parent, SWT.NONE);
		GridData searchRowLD = new GridData(SWT.FILL, SWT.NONE, true, false);
		searchRow.setLayoutData(searchRowLD);
		GridLayout searchRowLayout = new GridLayout(3, false);
		searchRow.setLayout(searchRowLayout);
		
		Text searchText = new Text(searchRow, SWT.BORDER);
		GridData searchLD = new GridData(SWT.FILL, SWT.NONE, true, true);
		searchText.setLayoutData(searchLD);
		searchText.addModifyListener(new ModifyListener(){
			@Override
			public void modifyText(ModifyEvent event) {
				
			}
		});
		
		Button prevButton = new Button(searchRow, SWT.BORDER);
		prevButton.setText("Prev");
		GridData prevLD = new GridData(SWT.RIGHT, SWT.NONE, false, true);
		prevButton.setLayoutData(prevLD);
		prevButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				int currentOffset = source.getSelectedRange().x;
				if (currentOffset <= 0) {
					currentOffset = document.getLength();
				}
				int findPosition = source.getFindReplaceTarget()
						.findAndSelect(currentOffset, searchText.getText(), false, false, false);
				if (findPosition < 0) {
					if (currentOffset < document.getLength()) {
						// search from the end
						currentOffset = document.getLength();
						findPosition = source.getFindReplaceTarget()
								.findAndSelect(currentOffset, searchText.getText(), false, false, false);
					} else {
						return;
					}
				}
			}
		});
		
		Button nextButton = new Button(searchRow, SWT.BORDER);
		nextButton.setText("Next");
		GridData nextLD = new GridData(SWT.RIGHT, SWT.NONE, false, true);
		nextButton.setLayoutData(nextLD);
		nextButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent event) {
				int currentOffset = source.getSelectedRange().x + source.getSelectedRange().y;
				int findPosition = source.getFindReplaceTarget()
						.findAndSelect(currentOffset, searchText.getText(), true, false, false);
				if (findPosition < 0) {
					if (currentOffset > 0) {
						// search from the start
						currentOffset = 0;
						findPosition = source.getFindReplaceTarget()
								.findAndSelect(currentOffset, searchText.getText(), true, false, false);
					} else {
						return;
					}
				}
			}
		});
	}

	private void loadFile(LogFile logFile, SourceViewer text) {
		if (logFile != null) {
			
	        LogTailerListener listener = new LogTailerListener(text);
	        Runnable tailer = null;
	        switch(logFile.getFileSystem()) {
	        case LOCAL:
	        	tailer = new LocalTailer(logFile.getPath(), listener, 5000);
	        	break;
	        case SFTP:
	        	try {
		        	JSch jsch = new JSch();
		            Session session = jsch.getSession(logFile.getUsername(), logFile.getHost());
		            session.setPassword(logFile.getPassword());
		            session.setConfig("StrictHostKeyChecking", "no");
		            tailer = new SftpTailer(session, logFile.getPath(), listener, 5000);
	        	} catch (Exception e) {
	        		e.printStackTrace();
	        	}
	        	break;
	        }
	        if (tailer != null) {
		        tailerTread = new Thread(tailer);
		        tailerTread.setName(logFile.getName());
		        tailerTread.setDaemon(true); // optional
		        tailerTread.start();
		        LOG.info("starting");
	        }
		}
	}

	@PreDestroy
	public void destroy() {
		if (tailerTread != null) {
			tailerTread.interrupt();
		}
	}
}

