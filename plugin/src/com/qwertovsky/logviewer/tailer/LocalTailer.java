package com.qwertovsky.logviewer.tailer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.qwertovsky.logviewer.listeners.LogTailerListener;

public class LocalTailer implements Runnable {
	private static final Logger LOG = LogManager.getLogger(LocalTailer.class);

    private static final int DEFAULT_DELAY_MILLIS = 5000;

    private static final int DEFAULT_BUFSIZE = 1024*1024*10;

    /**
     * Buffer on top of RandomAccessFile.
     */
    private final byte inbuf[];

    /**
     * The file which will be tailed.
     */
    private File file;

    /**
     * The amount of time to wait for the file to be updated.
     */
    private final long delayMillis;

    /**
     * Whether to tail from the end or start of file
     */
    private final boolean end;

    /**
     * The listener to notify of events when tailing.
     */
    private final LogTailerListener listener;

    /**
     * The tailer will run as long as this value is true.
     */
    private volatile boolean run = true;


    /**
     * Creates a Tailer for the given file, starting from the beginning. Charset and delay are default.
     * @param path file path on remote server
     * @param listener 
     */
    public LocalTailer(String path
    		, final LogTailerListener listener) {
        this(path, listener, DEFAULT_DELAY_MILLIS);
    }

    /**
     * Creates a Tailer for the given file, starting from the beginning.
     * @param path file path on remote server
     * @param listener
     * @param delayMillis delay before a reading new lines from file
     */
    public LocalTailer(String path
    		, final LogTailerListener listener
    		, final long delayMillis) {
    	this.file = new File(path);
        this.delayMillis = delayMillis;
        this.end = false;

        this.inbuf = new byte[DEFAULT_BUFSIZE];

        // Save and prepare the listener
        this.listener = listener;
    }

    /**
     * Gets whether to keep on running.
     *
     * @return whether to keep on running.
     * @since 2.5
     */
    protected boolean getRun() {
        return run;
    }

    /**
     * Return the delay in milliseconds.
     *
     * @return the delay in milliseconds.
     */
    public long getDelay() {
        return delayMillis;
    }

    /**
     * Follows changes in the file, calling the TailerListener's handle method for each new line.
     */
    public void run() {
    	RandomAccessFile reader = null;
        try {
            long last = 0; // The last time the file was checked for changes
            long position = 0; // position within the file
            // Open the file
            while (getRun() && reader == null) {
                try {
                	reader = new RandomAccessFile(file, "r");
                } catch (final FileNotFoundException e) {
                    listener.fileNotFound();
                }
                if (reader == null) {
                    Thread.sleep(delayMillis);
                } else {
                    // The current position in the file
                    position = end ? file.length() : 0;
                    last = file.lastModified();
                    reader.seek(position);
                }
            }
            while (getRun()) {
                final boolean newer = file.lastModified() > last; // IO-279, must be done first
                // Check the file length to see if it was rotated
                final long length = file.length();
                last = file.lastModified();
                if (length < position) {
                    // File was rotated
                	LOG.info("File was rotated. length={}, position={}, newer={}", length, position, newer);
                    listener.fileRotated();
                    position = 0;
                    continue;
                } else {
                    // File was not rotated
                    // See if the file needs to be read again
                    if (length > position) {
                        // The file has more content than it did last time
                    	reader = new RandomAccessFile(file, "r");
                    	reader.seek(position);
                    	LOG.debug("Need read. length={}, position={}", length, position);
                        position = readLines(reader, position);
                    }
                }
                closeReader(reader);
                Thread.sleep(delayMillis);
            }
        } catch (final InterruptedException e) {
            Thread.currentThread().interrupt();
            stop();
        } catch (final Exception e) {
            stop(e);
        } finally {
            closeReader(reader);
        }
    }

	private void closeReader(RandomAccessFile reader) {
		if (reader != null) {
			try {
				reader.close();
			} catch (Exception e) {}
		}
	}

    /**
     * Stops the tailer with an exception
     * @param e The exception to send to listener
     */
    private void stop(final Exception e) {
        listener.handle(e);
        stop();
    }

    /**
     * Allows the tailer to complete its current loop and return.
     */
    public void stop() {
        this.run = false;
    }

    /**
     * Read new lines.
     *
     * @param reader The file to read
     * @return The new position after the lines have been read
     * @throws java.io.IOException if an I/O error occurs.
     */
    private long readLines(final RandomAccessFile reader, long position) throws IOException {
    	int newBytes = 0;
        int num;
        while (getRun() && ((num = reader.read(inbuf)) != -1)) {
        	listener.handle(inbuf, 0, num);
            newBytes += num;
        }
        position += newBytes;
        LOG.debug("Readed lines. bytes={}, position={}", newBytes, position);

        listener.endOfFileReached();

        return position;
    }

}
