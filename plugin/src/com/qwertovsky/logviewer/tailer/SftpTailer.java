package com.qwertovsky.logviewer.tailer;

import java.io.IOException;
import java.io.InputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpATTRS;
import com.jcraft.jsch.SftpException;
import com.qwertovsky.logviewer.listeners.LogTailerListener;

public class SftpTailer implements Runnable {
	private static final Logger LOG = LogManager.getLogger(SftpTailer.class);
    private static final int DEFAULT_DELAY_MILLIS = 5000;

    private static final int DEFAULT_BUFSIZE = 1024*1024*10;

    /**
     * Buffer
     */
    private final byte inbuf[];

    /**
     * The amount of time to wait for the file to be updated.
     */
    private final long delayMillis;

    /**
     * Whether to tail from the end or start of file
     */
    private final boolean end;

    /**
     * The listener to notify of events when tailing.
     */
    private final LogTailerListener listener;

    /**
     * The tailer will run as long as this value is true.
     */
    private volatile boolean run = true;

    private ChannelSftp sftp;
    private Session session;
    private String filePath; 


    /**
     * Creates a Tailer for the given file, starting from the beginning. Delay is default.
     * @param session sftp session
     * @param path file path on remote server
     * @param listener 
     * @throws JSchException
     * @throws SftpException
     */
    public SftpTailer(final Session session, String path
    		, final LogTailerListener listener)
            throws JSchException, SftpException {
        this(session, path, listener, DEFAULT_DELAY_MILLIS);
    }


    /**
     * Creates a Tailer for the given file, starting from the beginning.
     * @param session sftp session
     * @param path file path on remote server
     * @param listener
     * @param delayMillis delay before a reading new lines from file
     * @throws JSchException
     * @throws SftpException
     */
    public SftpTailer(final Session session, String path
    		, final LogTailerListener listener
    		, final long delayMillis)
            throws JSchException, SftpException {
        this.filePath = path;
        this.session = session;
        this.delayMillis = delayMillis;
        this.end = false;

        this.inbuf = new byte[DEFAULT_BUFSIZE];

        // Save and prepare the listener
        this.listener = listener;
    }

    /**
     * Gets whether to keep on running.
     *
     * @return whether to keep on running.
     * @since 2.5
     */
    protected boolean getRun() {
        return run;
    }

    /**
     * Return the delay in milliseconds.
     *
     * @return the delay in milliseconds.
     */
    public long getDelay() {
        return delayMillis;
    }

    /**
     * Follows changes in the file, calling the LogTailerListener's handle method for each new line.
     */
    public void run() {
        // The file which will be tailed.
        InputStream file = null;
        try {
            long last = 0; // The last time the file was checked for changes
            long position = 0; // position within the file
            // Open the file
            if (file == null) {
                session.connect();
                sftp = (ChannelSftp)session.openChannel("sftp");
                sftp.connect();
                SftpATTRS attrs = sftp.stat(filePath);
                // The current position in the file
                position = end ? attrs.getSize() : 0;
                last = attrs.getMTime();
            }
            while (getRun()) {
                SftpATTRS attrs = sftp.stat(filePath);
                final boolean newer = attrs.getMTime() > last; 
                last = attrs.getMTime();
                // Check the file length to see if it was rotated
                final long length = attrs.getSize();
                if (length < position) {
                    // File was rotated
                	LOG.info("File was rotated. length={}, position={}, newer={}", length, position, newer);
                    listener.fileRotated();
                    position = 0;
                    continue;
                } else {
                    // File was not rotated
                    // See if the file needs to be read again
                    if (length > position) {
                        // The file has more content than it did last time
                    	file = sftp.get(filePath);
                    	LOG.debug("Need read. length={}, position={}", length, position);
                        position = readLines(file, position);
                        
                    }
                }
                closeReader(file);

                Thread.sleep(delayMillis);
            }
        } catch (final InterruptedException e) {
            Thread.currentThread().interrupt();
            stop();
        } catch (final Exception e) {
            stop(e);
        } finally {
            closeReader(file);
            session.disconnect();
            sftp.disconnect();
        }
    }

	private void closeReader(InputStream reader) {
		if (reader != null) {
			try {
				reader.close();
			} catch (Exception e) {}
		}
	}

    /**
     * Stops the tailer with an exception
     * @param e The exception to send to listener
     */
    private void stop(final Exception e) {
        listener.handle(e);
        stop();
    }

    /**
     * Allows the tailer to complete its current loop and return.
     */
    public void stop() {
        this.run = false;
    }

    /**
     * Read new lines.
     *
     * @param reader The file to read
     * @return The new position after the lines have been read
     * @throws java.io.IOException if an I/O error occurs.
     */
    private long readLines(final InputStream reader, long position) throws IOException {
        int newBytes = 0;
    	int num;
        reader.skip(position);
        while (getRun() && ((num = reader.read(inbuf)) != -1)) {
        	listener.handle(inbuf, 0, num);
            newBytes += num;
        }
        position += newBytes;
        LOG.debug("Readed lines. bytes={}, position={}", newBytes, position);
        listener.endOfFileReached();

        return position;
    }

}
