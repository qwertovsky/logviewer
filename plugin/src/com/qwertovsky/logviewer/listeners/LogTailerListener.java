package com.qwertovsky.logviewer.listeners;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;

import org.eclipse.jface.text.source.SourceViewer;
import org.eclipse.swt.widgets.Display;

import com.qwertovsky.logviewer.LogDocument;

public class LogTailerListener {
	
	private SourceViewer sourceViewer;
	private ByteArrayOutputStream buffer = new ByteArrayOutputStream();

	public LogTailerListener(SourceViewer sourceViewer) {
		this.sourceViewer = sourceViewer;
	}

	public void fileNotFound() {
		// TODO Auto-generated method stub

	}

	public void fileRotated() {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				LogDocument	document = (LogDocument) sourceViewer.getDocument();
				document.set("");
			}
		});
	}

	public void handle(byte[] buf, int offset, int length) {
		buffer.write(buf, offset, length);
	}

	public void endOfFileReached() throws UnsupportedEncodingException {
		final String newContent = buffer.toString(StandardCharsets.UTF_8.name());
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				LogDocument	document = (LogDocument) sourceViewer.getDocument();
				document.append(newContent);
			}
		});
		buffer = new ByteArrayOutputStream();
		System.gc();
    }

	public void handle(Exception e) {
		e.printStackTrace();
	}

}
