package com.qwertovsky.logviewer;

import org.eclipse.jface.text.AbstractDocument;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.DefaultLineTracker;
import org.eclipse.jface.text.DocumentEvent;
import org.eclipse.jface.text.ITextStore;

public class LogDocument extends AbstractDocument {

	private long modificationStamp;

	public LogDocument(ITextStore textStore) {
		setTextStore(textStore);
		setLineTracker(new DefaultLineTracker());
		completeInitialization();
	}

	/**
	 * Append text to the end
	 * @param text
	 * @throws BadLocationException 
	 */
	public void append(String newContent)  {
		int length = getLength();
//		try {
			DocumentEvent event = new DocumentEvent(this, length, 0, newContent);
			fireDocumentAboutToBeChanged(event);
	
			if (getStore() instanceof TailedTextStore) {
				((TailedTextStore)getStore()).append(newContent);
			} else {
				getStore().replace(length, 0, newContent);
			}
//			getTracker().replace(length, 0, newContent);
			getTracker().set(get());
			
			modificationStamp = System.currentTimeMillis();
			event.fModificationStamp = modificationStamp; 
			fireDocumentChanged(event);
//		} catch (BadLocationException e) {
//			e.printStackTrace();
//		}
	}

	@Override
	public long getModificationStamp() {
		return modificationStamp;
	}
	
}
