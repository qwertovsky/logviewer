package com.qwertovsky.logviewer.services.internal;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.preferences.IEclipsePreferences;
import org.eclipse.core.runtime.preferences.InstanceScope;
import org.osgi.service.prefs.BackingStoreException;

import com.qwertovsky.logviewer.model.FileSystemType;
import com.qwertovsky.logviewer.model.ILogViewerService;
import com.qwertovsky.logviewer.model.LogFile;
import com.qwertovsky.logviewer.model.PreferencesConstants;

public class LogViewerServiceImpl implements ILogViewerService {
	private ArrayList<LogFile> logFiles;
	private long lastId = 0;

	@SuppressWarnings("unchecked")
	public LogViewerServiceImpl() {
		IEclipsePreferences preferencies = InstanceScope.INSTANCE.getNode(PreferencesConstants.NODEPATH);
		byte[] filesBytes = preferencies.getByteArray(PreferencesConstants.FILES, null);
		if (filesBytes != null) {
			try {
				ByteArrayInputStream filesIn = new ByteArrayInputStream(filesBytes);
				ObjectInputStream filesOIn = new ObjectInputStream(filesIn);
				logFiles = (ArrayList<LogFile>) filesOIn.readObject();
			} catch (IOException | ClassNotFoundException e) {
				e.printStackTrace();
			}
		} else {
			logFiles = new ArrayList<>();
			LogFile logFile = new LogFile();
			logFile.setId(++lastId);
			logFile.setPath("/media/disk/Program_Files/wildfly-10.1.0-money/standalone/log/server.log");
			logFile.setName("money server");
			logFile.setFileSystem(FileSystemType.LOCAL);
			logFiles.add(logFile);
			
			logFile = new LogFile();
			logFile.setId(++lastId);
			logFile.setPath("/opt/wildfly/standalone/log/server.log");
			logFile.setHost("172.17.0.2");
			logFile.setName("money server sftp");
			logFile.setFileSystem(FileSystemType.SFTP);
			logFile.setUsername("root");
			logFile.setPassword("passvv0rd");
			logFiles.add(logFile);
		}
		for (LogFile file : logFiles) {
			if (file.getId() > lastId) {
				lastId = file.getId();
			}
		}
	}

	@Override
	public LogFile getLogFile(long id) {
		LogFile logFile = findById(id);
		if (logFile != null) {
			return logFile.copy();
		}
		return null;
	}

	@Override
	public synchronized void saveLogFile(LogFile logFile) {
		if (logFile == null) {
			throw new IllegalArgumentException("logFile can't be null");
		}
		if (logFile.getId() != null) {
			LogFile updatedFile = findById(logFile.getId());
			if (updatedFile == null) {
				throw new IllegalArgumentException("id does not exist");
			}
			logFiles.remove(updatedFile);
			LogFile newFile = logFile.copy();
			logFiles.add(newFile);
		} else {
			LogFile newFile = logFile.copy();
			newFile.setId(++lastId);
			logFiles.add(newFile);
		}
		
		saveFileList();
	}

	@Override
	public void deleteLogFile(long id) {
		LogFile deletedFile = findById(id);
		logFiles.remove(deletedFile );

		saveFileList();
	}

	@Override
	public List<LogFile> getLogFiles() {
		List<LogFile> files = new ArrayList<>();
		for (LogFile file : logFiles) {
			files.add(file.copy());
		}
		return files;
	}

	private LogFile findById(long id) {
		for (LogFile lf : logFiles) {
			if (lf.getId() == id) {
				return lf;
			}
		}
		return null;
	}

	public void saveFileList() {
		IEclipsePreferences preferencies = InstanceScope.INSTANCE.getNode(PreferencesConstants.NODEPATH);
		try {
			ByteArrayOutputStream filesOut = new ByteArrayOutputStream();
			ObjectOutputStream filesOOut = new ObjectOutputStream(filesOut);
			filesOOut.writeObject(logFiles);
			byte[] filesBytes = filesOut.toByteArray();
			preferencies.putByteArray(PreferencesConstants.FILES, filesBytes);
			preferencies.flush();
		} catch (IOException | BackingStoreException e) {
			e.printStackTrace();
		}
	}
}
