package com.qwertovsky.logviewer.services;

import com.qwertovsky.logviewer.model.ILogViewerService;
import com.qwertovsky.logviewer.services.internal.LogViewerServiceImpl;

public class LogViewerServiceFactory {
	private static ILogViewerService logViewerService = new LogViewerServiceImpl();

	public static ILogViewerService getInstance() {
		return logViewerService;
	}
}
