package com.qwertovsky.logviewer.model;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;

public class LogFile implements Serializable {
	private static final int DEFAULT_SFTP_PORT = 22;
	private static final long serialVersionUID = 1L;
	
	private PropertyChangeSupport changes = new PropertyChangeSupport(this);

	private Long id;
	private String path = null;
	private String host = null;
	private int port = DEFAULT_SFTP_PORT;
	private String name = null;
	private FileSystemType fileSystem;
	private String username;
	private String password;
	
	public LogFile() {
	}

	public LogFile(LogFile file) {
		this.id = file.id;
		this.path = file.path;
		this.name = file.name;
		this.fileSystem = file.fileSystem;
		this.username = file.username;
		this.password = file.password;
		this.host = file.host;
		this.port = file.port;
	}

	public LogFile copy() {
		return new LogFile(this);
	}
	
	public void clearPathInfo() {
		setPath(null);
		setHost(null);
		setPort(DEFAULT_SFTP_PORT);
		setUsername(null);
		setPassword(null);
	}

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		changes.firePropertyChange("id", this.id, id);
		this.id = id;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		changes.firePropertyChange("path", this.path, this.path = path);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		changes.firePropertyChange("name", this.name, name);
		this.name = name;
	}

	public FileSystemType getFileSystem() {
		return fileSystem;
	}

	public void setFileSystem(FileSystemType fileSystem) {
		changes.firePropertyChange("fileSystem", this.fileSystem, fileSystem);
		this.fileSystem = fileSystem;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		changes.firePropertyChange("username", this.username, username);
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		changes.firePropertyChange("password", this.password, password);
		this.password = password;
	}

	public String getHost() {
		return host;
	}

	public void setHost(String host) {
		changes.firePropertyChange("host", this.host, host);
		this.host = host;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		changes.firePropertyChange("port", this.port, port);
		this.port = port;
	}
	
	public void addPropertyChangeListener(PropertyChangeListener listener) {
		changes.addPropertyChangeListener(listener);
	}
	
	public void removePropertyChangeListener(PropertyChangeListener listener) {
		changes.removePropertyChangeListener(listener);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((path == null) ? 0 : path.hashCode());
		result = prime * result + ((fileSystem == null) ? 0 : fileSystem.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogFile other = (LogFile) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (path == null) {
			if (other.path != null)
				return false;
		} else if (!path.equals(other.path))
			return false;
		if (fileSystem == null) {
			if (other.fileSystem != null)
				return false;
		} else if (fileSystem != other.fileSystem)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return String.format("LogFile [id=%s, path=%s, name=%s, fileSystem=%s]", id, path, name, fileSystem);
	}
	
	
	
}
