package com.qwertovsky.logviewer.model;

import java.util.List;

public interface ILogViewerService {
	LogFile getLogFile(long id);
	void saveLogFile(LogFile logFile);
	void deleteLogFile(long id);
	List<LogFile> getLogFiles();
	void saveFileList();
}
